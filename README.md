# dotfiles
This repository is intended to keep a backup of my configs and scripts

<details>
  <summary> old linux config stuff </summary>
  
## List of configs  
- [My alacritty config](.config/alacritty/alacritty.yml)
- [My vim config](.config/nvim/init.vim)
- [My ranger config](.config/ranger/rc.conf)
- [My zsh config](.config/zsh/.zshrc)

## List of zsh scripts
- [gitdir](.local/bin/gitdir)
- [mkcprj](.local/bin/mkcprj) 
- [booksplit](.local/bin/booksplit) (taken from lukesmithxyz)
- [tag](.local/bin/tag) (taken from lukesmithxyz)

</details>

<details open>
  <summary> for windows </summary>
  
   + <details>
      <summary> vs code config </summary>

      ```json
      {
        "workbench.colorTheme": "Monokai Vibrant",
        "editor.fontFamily": "JetBrains Mono, Consolas, 'Courier New', monospace",
        "editor.fontSize": 17,
        "terminal.integrated.cwd": "${fileDirname}",
        "telemetry.enableTelemetry": false,
        "terminal.integrated.profiles.windows": {
          "PowerShell": {
            "source": "PowerShell",
            "args": [
              "-nologo"
            ]
          }
        },
        "terminal.integrated.defaultProfile.windows": "PowerShell",
        "workbench.iconTheme": "vscode-icons",
        "[cpp]": {
          "editor.defaultFormatter": "ms-vscode.cpptools",
          "editor.formatOnType": true
        },
        "cph.general.defaultLanguage": "cpp",
        "cph.general.defaultLanguageTemplateFileLocation": "D:\\Cpp-projects\\c++\\CompetitiveProgramming\\misc\\template.cpp",
        "cph.general.saveLocation": "D:\\misc\\utility",
        "cSpell.userWords": [
          "Collatz",
          "fastio"
        ],
        "[markdown]": {
          "editor.wordWrap": "on",
          "editor.quickSuggestions": false,
          "editor.defaultFormatter": "mervin.markdown-formatter",
          "editor.formatOnSave": true
        },
        "workbench.startupEditor": "newUntitledFile",
        "workbench.editor.untitled.hint": "hidden"
      }
      ```

      </details>
  
  + <details> 
      <summary> List of vscode extension </summary>
    
      CoenraadS.bracket-pair-colorizer-2
  
      DivyanshuAgrawal.competitive-programming-helper
  
      formulahendry.code-runner
  
      mervin.markdown-formatter
  
      ms-vscode.cpptools
  
      ms-vscode.cpptools-themes
  
      oderwat.indent-rainbow
  
      s3gf4ult.monokai-vibrant
  
      streetsidesoftware.code-spell-checker
  
      vscode-icons-team.vscode-icons
  
      yzhang.markdown-all-in-one
    

  </details>

   + <details>
      <summary> powershell config </summary>

      ```powershell
      function global:prompt {
        $Success = $?

        ## Time calculation
        $LastExecutionTimeSpan = if (@(Get-History).Count -gt 0) {
            Get-History | Select-Object -Last 1 | ForEach-Object {
                New-TimeSpan -Start $_.StartExecutionTime -End $_.EndExecutionTime
            }
        }
        else {
            New-TimeSpan
        }

        $LastExecutionShortTime = if ($LastExecutionTimeSpan.Days -gt 0) {
            "$($LastExecutionTimeSpan.Days + [Math]::Round($LastExecutionTimeSpan.Hours / 24, 2)) d"
        }
        elseif ($LastExecutionTimeSpan.Hours -gt 0) {
            "$($LastExecutionTimeSpan.Hours + [Math]::Round($LastExecutionTimeSpan.Minutes / 60, 2)) h"
        }
        elseif ($LastExecutionTimeSpan.Minutes -gt 0) {
            "$($LastExecutionTimeSpan.Minutes + [Math]::Round($LastExecutionTimeSpan.Seconds / 60, 2)) m"
        }
        elseif ($LastExecutionTimeSpan.Seconds -gt 0) {
            "$($LastExecutionTimeSpan.Seconds + [Math]::Round($LastExecutionTimeSpan.Milliseconds / 1000, 2)) s"
        }
        elseif ($LastExecutionTimeSpan.Milliseconds -gt 0) {
            "$([Math]::Round($LastExecutionTimeSpan.TotalMilliseconds, 2)) ms"
        }
        else {
            "0 s"
        }

        if ($Success) {
            Write-Host -Object "[$LastExecutionShortTime] " -NoNewline -ForegroundColor Green
        }
        else {
            Write-Host -Object "! [$LastExecutionShortTime] " -NoNewline -ForegroundColor Red
        }

        ## History ID
        $HistoryId = $MyInvocation.HistoryId
        Write-Host -Object "$HistoryId`: " -NoNewline -ForegroundColor Cyan

        ## Path
        $PwdPath = Split-Path -Path $pwd -Leaf
        Write-Host -Object "$PwdPath" -NoNewline -ForegroundColor Magenta

        return "> "
      }
      ```

      </details>
  
</details>
